//
//  SwiftGraphviz.h
//  SwiftGraphviz
//
//  Created by Klaus Kneupner on 30/08/2020.
//  Copyright © 2020 Klaus Kneupner. All rights reserved.
//

#import <Foundation/Foundation.h>

#include "gvplugin.h" //
#include "gvc.h"
#include "builtins.h"
#include "unflatten.h"

//! Project version number for SwiftGraphviz.
FOUNDATION_EXPORT double SwiftGraphvizVersionNumber;

//! Project version string for SwiftGraphviz.
FOUNDATION_EXPORT const unsigned char SwiftGraphvizVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SwiftGraphviz/PublicHeader.h>


